# PHP Example

A sample php microservice block for [kintohub](http://kintohub.com)

# First time setup

* run `compose install`

# Run

* `php -S localhost:8000` run on port 8000

# IMPORTANT
  this example project is built using using docker file from this repo which
  may contain version of language different from the one you've set in Kintohub's UI

  If you want to use the version you've set in Kintohub's UI -> just remove the Dockerfile

# Support

http://www.kintohub.com
